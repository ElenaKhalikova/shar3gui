﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SHAR_v3
{
    //[Serializable]
    class Structure
    {
        public string Name { get; set; }
        public double Value { get; set; }

        public Structure(string n, double d)
        {
            Name = n;
            Value = d;
        }
        public Structure()
        {
            Name = "";
            Value = 0.0;
        }


    }

}
