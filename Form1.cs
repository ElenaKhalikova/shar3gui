﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SHAR_v3
{
    public partial class Form1 : Form
    {
        List<Structure> ArraySHAR { get; set; }
        List<Structure> ArrayIN { get; set; }
        public Form1()
        {
            InitializeComponent();
            ArraySHAR = new List<Structure>();
            ArrayIN = new List<Structure>();
            Init1();
            Init2();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void импортИзSHARDATToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string oldPath = Directory.GetCurrentDirectory();
            string sharPath = oldPath + @"\shar\";
            Directory.SetCurrentDirectory(sharPath);
            OpenFileDialog OPF = new OpenFileDialog();
            OPF.InitialDirectory = System.IO.Path.Combine(Application.StartupPath, sharPath);
            OPF.Filter = "Data file (shar.dat)|shar.dat";
            if (OPF.ShowDialog() == DialogResult.OK)
            {
                // заполнение ArraySHAR
                ArraySHAR.Clear();
                // чтение из файла SHAR.DAT               
                var text = File.ReadAllText(OPF.FileName, Encoding.GetEncoding(866));
                Structure s;
                string str;
                for (int i = 2; i < 81; i++)
                {
                    if (text.Substring(65 + 73 * (i - 1), 1) == "E" && text.Substring(66 + 73 * (i - 1), 1) == " ")
                        text = text.Remove(66 + 73 * (i - 1), 1).Insert(66 + 73 * (i - 1), '+'.ToString());
                    str = text.Substring(56 + 73 * (i - 1), 13).Trim();
                    if (str.Length == 0) str = "0.0";
                    str = str.Replace('.', ',');
                    s = new Structure(text.Substring(2 + 73 * (i - 1), 54), Convert.ToDouble(str));
                    ArraySHAR.Add(s);
                }
                dataGridView1.DataSource = ArraySHAR;
                dataGridView1.Columns[0].Width = 475;
            }
            Directory.SetCurrentDirectory(oldPath);
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void Init1()
        {
            // Инициализация первой таблицы
            string oldPath = Directory.GetCurrentDirectory();
            string sharPath = oldPath + @"\shar\SHAR.DAT";
            // заполнение ArraySHAR
            ArraySHAR.Clear();
            // чтение из файла SHAR.DAT               
            var text = File.ReadAllText(sharPath, Encoding.GetEncoding(866));
            Structure s;
            string str;
            for (int i = 2; i < 81; i++)
            {
                if (text.Substring(65 + 73 * (i - 1), 1) == "E" && text.Substring(66 + 73 * (i - 1), 1) == " ")
                    text = text.Remove(66 + 73 * (i - 1), 1).Insert(66 + 73 * (i - 1), '+'.ToString());
                str = text.Substring(56 + 73 * (i - 1), 13).Trim();
                if (str.Length == 0) str = "0.0";
                str = str.Replace('.', ',');
                s = new Structure(text.Substring(2 + 73 * (i - 1), 54), Convert.ToDouble(str));
                ArraySHAR.Add(s);
            }
            dataGridView1.DataSource = ArraySHAR;
            dataGridView1.Columns[0].Width = 475;
            Directory.SetCurrentDirectory(oldPath);
        }

        private void Init2()
        {
            // Инициализация второй таблицы
            ArrayIN.Clear();
            Structure s;
            for (int i = 0; i < 16; i++)
            {
                s = new Structure(GlobalString.ArrayListdataGridView2[i], 1);
                ArrayIN.Add(s);
            }
            dataGridView2.DataSource = ArrayIN;
            dataGridView2.Columns[0].Width = 475;
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Open data file (*.dat)|*.dat";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string jsonString = File.ReadAllText(openFileDialog1.FileName);
                ArraySHAR = JsonSerializer.Deserialize<List<Structure>>(jsonString);
                //Structure obj;
                //BinaryReader streamReader = new BinaryReader((File.Open(openFileDialog1.FileName, FileMode.Open)));
                //FileStream f = new FileStream(openFileDialog1.FileName, FileMode.Open);

                //try
                //{



                //// чтение из файла dataGridView1
                //for (int i = 0; i < dataGridView1.Rows.Count; i++)
                //{
                //    dataGridView1.Rows[i].Cells[0].Value = streamReader.ReadString();
                //    dataGridView1.Rows[i].Cells[1].Value = streamReader.ReadDouble();
                //}
                // чтение из файла dataGridView2

                //BinaryFormatter form = new BinaryFormatter();
                //ArraySHAR = form.Deserialize(f);






                //    //streamReader.Close();
                //}
                //catch//(SerializationException ex)
                //{
                //   // MessageBox.Show(ex.Message);
                //}
                //finally
                //{
                //    f.Close();
                //}
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream f;
            //BinaryFormatter form;
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Save data file (*.dat)|*.dat";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //BinaryWriter streamWriter = new BinaryWriter(File.Open(saveFileDialog1.FileName, FileMode.OpenOrCreate));
                //f = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                //form = new BinaryFormatter();
                ////Structure ob = ArraySHAR[0];
                //try
                //{
                //    // запись в файл ArrayShar
                //    form.Serialize(f, ArraySHAR);
                //}
                //catch (SerializationException ex)
                //{
                //    MessageBox.Show(ex.Message);
                //}
                //finally
                //{
                //    f.Close();
                //}
            }
        }

        void runShar()
        {
            string oldPath = Directory.GetCurrentDirectory();
            string sharPath = oldPath + @"\shar\";
            Directory.SetCurrentDirectory(sharPath);
            using (Process myProcess = Process.Start("Shar12sm.exe"))
            {
                do
                {
                    Application.DoEvents();
                } while (!myProcess.WaitForExit(100));
            }
            Directory.SetCurrentDirectory(oldPath);
        }

        void SaveSharDAT()
        {
            string oldPath = Directory.GetCurrentDirectory();
            string sharPath = oldPath + @"\shar\SHAR.DAT";
            using (StreamWriter streamWriter = new StreamWriter(new FileStream(sharPath, FileMode.OpenOrCreate), Encoding.GetEncoding(866)))
                try
            {
                // записать наименование подшипника
                streamWriter.WriteLine(": TИП ПOДШИПHИKA  84-126308PЯУ                                        :");
                string str,buffer;
                // запись в файл SHAR.DAT данных из dataGridView1
                //for (int i = 0; i < dataGridView1.Rows.Count; i++)
                //{
                //    str = ": ";
                //    str += String.Format("{0, -55}", dataGridView1.Rows[i].Cells[0].Value);
                //    buffer = String.Format("{0, -13:G}", Double.Parse(dataGridView1.Rows[i].Cells[1].Value.ToString()));


                //        buffer = buffer.Replace(',', '.'); ;
                //    str += buffer;
                //    str += ":";
                //    streamWriter.WriteLine(str);
                //}
                    for (int i = 0; i < ArraySHAR.Count; i++)
                    {
                        str = ": ";
                        str += String.Format("{0, -55}",ArraySHAR[i].Name);
                        buffer = String.Format("{0, -13:G}", ArraySHAR[i].Value);


                        buffer = buffer.Replace(',', '.'); ;
                        str += buffer;
                        str += ":";
                        streamWriter.WriteLine(str);
                    }
                    streamWriter.Close();
                MessageBox.Show("Файл SHAR.DAT успешно сохранен");
            }
            catch
            {
                MessageBox.Show("Ошибка при сохранении файла!");
            }
            Directory.SetCurrentDirectory(oldPath);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // запись введенных данных из dataGridView1 в shar.dat
            //SaveSharDAT();
            // запуск программы Shar
            runShar();
            // создание новой формы
            Form2 tempDialog = new Form2();
            tempDialog.ShowDialog();
            // вывод в dataGridView3 результатов работы shar

            // сохранение всех данных в файл IN.dat

            // запуск shar2
            runShar2();
            // вывод в dataGridView4 результатов расчетов

        }

        void runShar2()
        {
            // буферный файл
            string oldPath = Directory.GetCurrentDirectory();
            string sharPath = oldPath + @"\shar\";
            Directory.SetCurrentDirectory(sharPath);
            FileStream fs = new FileStream(sharPath + @"TEMP.TXT", FileMode.Create);
            StreamWriter streamWriter = new StreamWriter(fs);
            streamWriter.WriteLine("Определение констант");
            try
            {
                // Определение констант
                const double ConstE0Shtrih = 1.085e11;
                const double ConstA = 1.4599e9;
                const double ConstZita = 0.3;
                const double ConstKsi = 0.786;
                const double Consth = (double)7 / 3;
                const double Conste = (double)10 / 9;
                const double Constc = (double)31 / 3;
                double ConstK = 2 * Math.Pow(ConstZita, Constc) * Math.Pow(ConstKsi, 1 - Consth);
                streamWriter.WriteLine(ConstE0Shtrih);
                streamWriter.WriteLine(ConstA);
                streamWriter.WriteLine(ConstZita);
                streamWriter.WriteLine(ConstKsi);
                streamWriter.WriteLine(Consth);
                streamWriter.WriteLine(Conste);
                streamWriter.WriteLine(Constc);
                streamWriter.WriteLine(ConstK);

                // Дополнительные переменные
                double Sum = 0; // сумма
                int i = 0, j = 0; // счетчик
                double data = 0;

                // Описание переменных
                double d = 1; // диаметр шарика
                double E1 = 1; // модуль упругости внутреннего кольца
                double E2 = 1; // модуль упругости шариков
                double Nu1 = 1; // коэффициент Пуассона внутреннего кольца
                double Nu2 = 1; // коэффициент Пуассона шариков

                double PdBK = 1; // посадочный диаметр внутреннего кольца
                double PdHK = 1; // посадочный диаметр наружного кольца

                int n = 1; // количество шариков

                double RadPDKBK = 1; // радиус профиля дорожки качения ВК
                double RadPDKHK = 1; // радиус профиля дорожки качения HК

                double nBK = 1; // частота вращения внутреннего кольца
                double nHK = 1; // частота вращения наружного кольца

                double Rashar = 0; // чистота обработки шарика
                double RaBK = 0; // чистота обработки BK
                double RaHK = 0; // чистота обработки HK
                double T = 0; // температура внутреннего кольца (3.5)

                double lambdaE = 0; // 1.17
                double PShtrihBK = 1; // ??? 1.21a
                double RadDKHK = 1; // радиус дорожки качения 1.23

                double[] AlphaBK = new double[2]; // 1.3
                double[] AlphaHK = new double[2]; // 1.4

                // расчетные данные
                double[] LBK = new double[2];
                double[] LHK = new double[2];
                double[] LsharBK = new double[2];
                double[] LsharHK = new double[2];

                streamWriter.WriteLine("Исходные данные");
                for (i = 0; i < 77; i++)
                {
                    data = Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                    //MessageBox.Show(Convert.ToString(data));
                    switch (i+2)
                    {
                        case 2: d = data / 100; streamWriter.WriteLine("d = {0}", d); break;
                        case 5: RadPDKBK = data / 100; streamWriter.WriteLine("RadPDKBK = {0}", RadPDKBK); break;
                        case 8: RadPDKHK = data / 100; RadDKHK = RadPDKHK; streamWriter.WriteLine("RadPDKHK = {0}", RadPDKHK); break;
                        case 20: n = (int)data; streamWriter.WriteLine("n = {0}", n); break;
                        case 24: nBK = data; streamWriter.WriteLine("nBK = {0}", nBK); break;
                        case 25: nHK = data; streamWriter.WriteLine("nHK = {0}", nHK); break;
                        case 26: PdBK = data / 100; streamWriter.WriteLine("PdBK = {0}", PdBK); break;
                        case 27: PdHK = data / 100; streamWriter.WriteLine("PdHK = {0}", PdHK); break;
                        case 40: E2 = data * 98100; streamWriter.WriteLine("E2 = {0}", E2); break;
                        case 41: E1 = data * 98100; streamWriter.WriteLine("E1 = {0}", E1); break;
                        case 46: Nu2 = data; streamWriter.WriteLine("Nu2 = {0}", Nu2); break;
                        case 47: Nu1 = data; streamWriter.WriteLine("Nu1 = {0}", Nu1); break;
                        case 64: T = data; streamWriter.WriteLine("T = {0}", T); break;
                        case 69: Rashar = data / 100; streamWriter.WriteLine("Rashar = {0}", Rashar); break;
                        case 70: RaBK = data / 100; streamWriter.WriteLine("RaBK = {0}", RaBK); break;
                        case 71: RaHK = data / 100; streamWriter.WriteLine("RaHK = {0}", RaHK); break;
                    }
                }

                StreamReader textRezsh = new StreamReader(sharPath + @"REZSH.DAT");
                // описание массивов данных из rezsh.dat
                double[,] AlphaiBK; AlphaiBK = new double[2, n]; // значений углов контакта шариков с внутренним кольцом
                double[,] AlphaiHK; AlphaiHK = new double[2, n]; //значений углов контакта шариков с наружним кольцом
                double[,] PHBK; PHBK = new double[2, n]; //значений контактных напряжений шарика на внутреннем кольце
                double[,] PHHK; PHHK = new double[2, n]; // значений контактных напряжений шарика на наружнем кольце
                double[,] QBK; QBK = new double[2, n]; // значений сил между шариками и внутренним кольцом 
                double[,] QHK; QHK = new double[2, n]; // значений сил между шариками и наружным кольцом
                double[,] hBK_in; hBK_in = new double[2, n]; // минимальная толщина пленки в контакте с внутренним кольцом
                double[,] hHK_in; hHK_in = new double[2, n]; // минимальная толщина пленки в контакте с наружним кольцом

                // считывание данных из файла rezsh.dat
                streamWriter.WriteLine("                  угол           сила           hBK");
                string str, buffer;
                // пропустить 238 строк до таблицы 4
                for (i = 1; i < 239; i++)
                    str = textRezsh.ReadLine();

                for (i = 0; i < n; i++)
                {
                    // заполнение значений контакта шариков с внутренним кольцом 1
                    str = textRezsh.ReadLine(); // считать строку из файла	
                    {
                        buffer = str.Substring(24, 4).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        AlphaiBK[0, i] = Convert.ToDouble(buffer) * Math.PI / 180;
                        buffer = str.Substring(10, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        QBK[0,i] = Convert.ToDouble(buffer) * 9.8100;
                        buffer = str.Substring(66, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        hBK_in[0,i] = Convert.ToDouble(buffer) / Math.Pow(10, 6);
                        streamWriter.WriteLine("{0}  BK1:  {1}       {2}    {3} ", i + 1, AlphaiBK[0, i], QBK[0, i], hBK_in[0, i]);
                    }
                    
                    // заполнение значений  контакта шариков с внутренним кольцом 2
                    str = textRezsh.ReadLine(); // считать строку из файла	
                    {
                        buffer = str.Substring(24, 4).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        AlphaiBK[1, i] = Convert.ToDouble(buffer) * Math.PI / 180;
                        buffer = str.Substring(10, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        QBK[1,i] = Convert.ToDouble(buffer) * 9.8100;
                        buffer = str.Substring(66, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        hBK_in[1,i] = Convert.ToDouble(buffer) / Math.Pow(10, 6);
                        streamWriter.WriteLine("{0}  BK2:  {1}       {2}    {3} ", i + 1, AlphaiBK[1, i], QBK[1, i], hBK_in[1, i]);
                    }

                    // заполнение значений  контакта шариков с наружным кольцом 1
                    str = textRezsh.ReadLine(); // считать строку из файла
                    {
                        buffer = str.Substring(24, 4).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        AlphaiHK[0, i] = Convert.ToDouble(buffer) * Math.PI / 180;
                        buffer = str.Substring(10, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        QHK[0,i] = Convert.ToDouble(buffer) * 9.8100;
                        buffer = str.Substring(66, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        hHK_in[0,i] = Convert.ToDouble(buffer) / Math.Pow(10, 6);
                        streamWriter.WriteLine("{0}  HK1:  {1}       {2}    {3} ", i + 1, AlphaiHK[0, i], QHK[0, i], hHK_in[0, i]);
                    }

                    // заполнение значений  контакта шариков с наружным кольцом 2
                    str = textRezsh.ReadLine(); // считать строку из файла	
                    {
                        buffer = str.Substring(24, 4).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        AlphaiHK[1, i] = Convert.ToDouble(buffer) * Math.PI / 180;
                        buffer = str.Substring(10, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        QHK[1, i] = Convert.ToDouble(buffer) * 9.8100;
                        buffer = str.Substring(66, 5).Trim();
                        if (buffer.Length == 0) str = "0.0";
                        buffer = buffer.Replace('.', ',');
                        hHK_in[1, i] = Convert.ToDouble(buffer) / Math.Pow(10, 6);
                        streamWriter.WriteLine("{0}  HK2:  {1}       {2}    {3} ", i + 1, AlphaiHK[1, i], QHK[1, i], hHK_in[1, i]);
                    }
                    str = textRezsh.ReadLine(); // считать строку из файла
                }

                // Задание начальных значений параметров
                streamWriter.WriteLine("Задание начальных значений параметров");
                double PhiBK = 1; // Эмпирические параметры долговечности внутреннего кольца
                double PhiHK = 1; // Эмпирические параметры долговечности наружного кольца
                double PhiSh = 1; // Эмпирические параметры долговечности шариков
                double HRC_BK = 0; // твердость внутреннего кольца
                double HRC_HK = 0; // твердость наружного кольца
                double HRC_shar = 0; // твердость шариков
                double TR = 0; // температура измерения твердости
                double FR = 1; // минимальный размер частиц
                double pbr = 1; // вероятность безотказной работы
                double type_BK = 1; // Материал внутреннего кольца
                double type_HK = 1; // Материал наружного кольца
                double type_shar = 1; // Материал шариков
                double method_BK = 1; // Способ выплавки внутреннего кольца
                double method_HK = 1; // Способ выплавки наружного кольца
                double method_shar = 1; // Способ выплавки шариков
                double process = 1; // Тип подшипника

                // заполнение данными из dataGridView2
                
                PhiBK = Convert.ToDouble(dataGridView2.Rows[0].Cells[1].Value); streamWriter.WriteLine("PhiBK = {0}", PhiBK);
                PhiHK = Convert.ToDouble(dataGridView2.Rows[1].Cells[1].Value); streamWriter.WriteLine("PhiHK = {0}", PhiHK);
                PhiSh = Convert.ToDouble(dataGridView2.Rows[2].Cells[1].Value); streamWriter.WriteLine("PhiSh = {0}", PhiSh);
                HRC_BK = Convert.ToDouble(dataGridView2.Rows[3].Cells[1].Value); streamWriter.WriteLine("HRC BK = {0}", HRC_BK);
                HRC_HK = Convert.ToDouble(dataGridView2.Rows[4].Cells[1].Value); streamWriter.WriteLine("HRC HK = {0}", HRC_HK);
                HRC_shar = Convert.ToDouble(dataGridView2.Rows[5].Cells[1].Value); streamWriter.WriteLine("HRC shar = {0}", HRC_shar);
                TR = Convert.ToDouble(dataGridView2.Rows[6].Cells[1].Value); streamWriter.WriteLine("TR = {0}", TR);
                FR = Convert.ToDouble(dataGridView2.Rows[7].Cells[1].Value); streamWriter.WriteLine("FR = {0}", FR);
                pbr = Convert.ToDouble(dataGridView2.Rows[8].Cells[1].Value); streamWriter.WriteLine("pbr = {0}", pbr);
                type_BK = Convert.ToDouble(dataGridView2.Rows[9].Cells[1].Value); streamWriter.WriteLine("type BK = {0}", type_BK);
                type_HK = Convert.ToDouble(dataGridView2.Rows[10].Cells[1].Value); streamWriter.WriteLine("type HK = {0}", type_HK);
                type_shar = Convert.ToDouble(dataGridView2.Rows[11].Cells[1].Value); streamWriter.WriteLine("type shar = {0}", type_shar);
                method_BK = Convert.ToDouble(dataGridView2.Rows[12].Cells[1].Value); streamWriter.WriteLine("method BK = {0}", method_BK);
                method_HK = Convert.ToDouble(dataGridView2.Rows[13].Cells[1].Value); streamWriter.WriteLine("method HK = {0}", method_HK);
                method_shar = Convert.ToDouble(dataGridView2.Rows[14].Cells[1].Value); streamWriter.WriteLine("method shar = {0}", method_shar);
                process = Convert.ToDouble(dataGridView2.Rows[15].Cells[1].Value); streamWriter.WriteLine("process = {0}", process);

                streamWriter.WriteLine();
                streamWriter.WriteLine("Результаты расчетов");
                
                // 1 Расчет индивидуальных номинальных долговечностей контактов
                for (j = 0; j < 2; j++)
                {
                    // 1.1 
                    double EShtrih = 1 / ((1 - Nu1 * Nu1) / E1 + (1 - Nu2 * Nu2) / E2); // дорожка качения
                    streamWriter.WriteLine("1.1: EShtrih = {0}", EShtrih);
                    
                    // 1.2
                    double dm = (PdBK + PdHK) / 2;
                    streamWriter.WriteLine("1.2: dm = {0}", dm);

                    // 1.3    
                    // расчет угла внутреннего контакта
                    for (i = 0; i < n; i++)
                        AlphaBK[j] += AlphaiBK[j,i];
                    AlphaBK[j] /= n;
                    streamWriter.WriteLine("1.3: AlphaBK = {0}", AlphaBK[j]);

                    // 1.4
                    // расчет угла наружного контакта
                    for (i = 0; i < n; i++)
                        AlphaHK[j] += AlphaiHK[j,i];
                    AlphaHK[j] /= n;
                    streamWriter.WriteLine("1.4 AlphaHK = {0}", AlphaHK[j]);

                    // 1.5
                    double GammaBK = d / dm * Math.Cos(AlphaBK[j]);
                    streamWriter.WriteLine("1.5: GammaBK = {0}", GammaBK);

                    // 1.6
                    double RoBK = 4 / d - 1 / RadPDKBK + 2 * Math.Cos(AlphaBK[j]) / (dm * (1 - GammaBK));
                    streamWriter.WriteLine("1.6: RoBK = {0}", RoBK);

                    // 1.7
                    double FofRo = (1 / RadPDKBK + 2 * Math.Cos(AlphaBK[j]) / (dm * (1 - GammaBK))) / RoBK;
                    streamWriter.WriteLine("1.7: FofRo = {0}", FofRo);

                    // 1.8
                    double bBK = 0.54436 - 0.8365 * (FofRo - 0.8);
                    streamWriter.WriteLine("1.8: bBK = {0}", bBK);

                    // 1.9
                    double aBK = (1.24742 + 1.77476 * (FofRo - 0.8)) / bBK;
                    streamWriter.WriteLine("1.9: aBK = {0}", aBK);

                    // 1.9a
                    streamWriter.WriteLine("1.9a");
                    for (i = 0; i < n; i++)
                    {
                        PHBK[j,i] = Math.Pow((double)3 / 2 * QBK[j,i], (double)1 / 3) * Math.Pow(RoBK * EShtrih, (double)2 / 3) / Math.PI / aBK / bBK;
                        streamWriter.WriteLine("PHBK[{0}] = {1}", i + 1, PHBK[j,i]);
                    }

                    // 1.10
                    double GammaHK = d / dm * Math.Cos(AlphaHK[j]);
                    streamWriter.WriteLine("1.10: GammaHK = {0}", GammaHK);

                    // 1.11
                    double ncen = (nBK * (1 - GammaBK) + nHK * (1 + GammaHK)) / 2;
                    streamWriter.WriteLine("1.11: ncen = {0}", ncen);

                    // 1.12
                    double uBK = 0;
                    if ((nHK == 0) && (nBK != 0))
                    {
                        uBK = Math.Abs((ncen - nBK) / nBK);
                    }

                    if ((nHK != 0) && (nBK == 0))
                    {
                        uBK = ncen / nHK;
                    }
                    streamWriter.WriteLine("1.12: uBK = {0}", uBK);

                    // 1.13, 1.14
                    double GBK = dm * (1 - GammaBK) * Math.Pow(1 / RoBK, 2 - Consth) * Math.Pow(aBK, 3 - Consth) * Math.Pow(bBK, 3 - 2 * Consth);
                    streamWriter.WriteLine("1.13 - 1.14: GBK = {0}", GBK);

                    // 1.15
                    lambdaE = EShtrih / ConstE0Shtrih;
                    streamWriter.WriteLine("1.15: lambdaE = {0}", lambdaE);
                    
                    // 1.16
                    double PShtrihHBK = 0;

                    // 1.16a
                    if (nHK == 0 && nBK != 0)
                    {
                        Sum = 0;
                        for (i = 0; i < n; i++)
                        {
                            Sum += Math.Pow(PHBK[j,i], (Constc + 2 - Consth) / Conste);
                        }
                        PShtrihHBK = Math.Pow(Sum / n, Conste / (Constc + 2 - Consth));
                        streamWriter.WriteLine("1.16a: PShtrihHBK = {0}", PShtrihHBK);
                    }

                    // 1.16b
                    if (nHK != 0 && nBK == 0)
                    {
                        Sum = 0;
                        for (i = 0; i < n; i++)
                        {
                            Sum += Math.Pow(PHBK[j,i], (Constc + 2 - Consth));
                        }
                        PShtrihHBK = Math.Pow(Sum / n, 1 / (Constc + 2 - Consth));
                        streamWriter.WriteLine("1.16b: PShtrihHBK = {0}", PShtrihHBK);
                    }

                    // 1.17
                    double PHCBK = 0;
                    PHCBK = PhiBK * ConstA * Math.Pow(ConstK, -1 / (Constc + 2 - Consth)) * Math.Pow(lambdaE, (2 - Consth) / (Constc + 2 - Consth)) * Math.Pow(GBK, -1 / (Constc + 2 - Consth)) * Math.Pow(uBK, -Conste / (Constc + 2 - Consth));
                    streamWriter.WriteLine("1.17: PHCBK = {0}", PHCBK);

                    double Ro254BK = 0;
                    double EpsBK = 0;
                    if (d > 0.0254)
                    {
                        Ro254BK = (double)d / 0.0254 * RoBK;
                        EpsBK = Math.Pow(ConstE0Shtrih * lambdaE * ConstZita * Ro254BK / (Math.PI * ConstKsi * aBK * bBK * bBK), (double)4 / 3);
                        PHCBK *= Math.Pow(1 / EpsBK, 1 / (Constc + 2 - Consth));
                        streamWriter.WriteLine("1.17: PHCBK = {0}", PHCBK);
                    }


                    // 1.18
                    LBK[j] = Math.Pow(PHCBK / PShtrihHBK, (Constc + 2 - Consth) / Conste);
                    streamWriter.WriteLine("1.18: LBK[{0}] = {1}", j + 1, LBK[j]);

                    // 1.19
                    double GsharBK = d * Math.Pow(1 / RoBK, 2 - Consth) * Math.Pow(aBK, 3 - Consth) * Math.Pow(bBK, 3 - 2 * Consth);
                    streamWriter.WriteLine("1.19: GsharBK = {0}", GsharBK);

                    // 1.20
                    double nshar = 0;
                    double ushar = 0;
                    // 1.20a
                    if (nHK == 0 && nBK != 0)
                    {
                        nshar = nBK * dm * (1 - GammaBK) / (2 * d);
                        ushar = Math.Abs((nshar + ncen) / nBK);
                    }

                    // 1.20b
                    if (nHK != 0 && nBK == 0)
                    {
                        nshar = nHK * dm * (1 + GammaHK) / (2 * d);
                        ushar = Math.Abs((nshar + ncen) / nHK);
                    }

                    streamWriter.WriteLine("1.20");
                    streamWriter.WriteLine("nshar = {0}", nshar);
                    streamWriter.WriteLine("ushar = {0}", ushar);

                    // 1.21
                    double PHCsharBK = 0;
                    PHCsharBK = PhiSh * ConstA * Math.Pow(ConstK, -1 / (Constc + 2 - Consth)) * Math.Pow(lambdaE, (2 - Consth) / (Constc + 2 - Consth)) * Math.Pow(GsharBK, -1 / (Constc + 2 - Consth)) * Math.Pow(ushar, -Conste / (Constc + 2 - Consth));
                    if (d > 0.0254)
                    {
                        PHCsharBK *= Math.Pow(1 / EpsBK, 1 / (Constc + 2 - Consth));
                    }
                    streamWriter.WriteLine("1.21: PHCsharBK = {0}", PHCsharBK);

                    double PShtrihHsharBK = 0;

                    // 1.21
                    if (nHK == 0 && nBK != 0)
                    {
                        PShtrihHsharBK = PShtrihHBK;
                    }
                    if (nHK != 0 && nBK == 0)
                    {
                        Sum = 0;
                        for (i = 0; i < n; i++)
                        {
                            Sum += Math.Pow(PHBK[j,i], (Constc + 2 - Consth) / Conste);
                        }
                        PShtrihHsharBK = Math.Pow(1 / n * Sum, (Conste / (Constc + 2 - Consth)));
                    }
                    streamWriter.WriteLine("1.21: PShtrihHsharBK = {0}", PShtrihHsharBK);

                    // 1.22
                    LsharBK[j] = Math.Pow(PHCsharBK / PShtrihHsharBK, (Constc + 2 - Consth) / Conste);
                    streamWriter.WriteLine("1.22: LsharBK[{0}] = {1}", j + 1, LsharBK[j]);

                    // 1.23
                    double RoHK = 0;
                    RoHK = 4 / d - 1 / RadDKHK - 2 * Math.Cos(AlphaHK[j]) / (dm * (1 + GammaHK));
                    streamWriter.WriteLine("1.23: RoHK = {0}", RoHK);
                   
                    // 1.24
                    double FHKofRo = 0;
                    FHKofRo = (1 / RadDKHK - 2 * Math.Cos(AlphaHK[j]) / (dm * (1 + GammaHK))) / RoHK;
                    streamWriter.WriteLine("1.24: FHKofRo = {0}", FHKofRo);

                    // 1.25
                    double bHK = 0.54436 - 0.8365 * (FHKofRo - 0.8);
                    streamWriter.WriteLine("1.25: bHK = {0}", bHK);

                    // 1.26
                    double aHK = (1.24742 + 1.77476 * (FHKofRo - 0.8)) / bHK;
                    streamWriter.WriteLine("1.26: aHK = {0}", aHK);

                    // 1.26a
                    streamWriter.WriteLine("1.26a");
                    for (i = 0; i < n; i++)
                    {
                        PHHK[j,i] = Math.Pow(3 / 2 * QHK[j,i], (double)1 / 3) * Math.Pow(RoHK * EShtrih, (double)2 / 3) / (Math.PI * aHK * bHK);
                        streamWriter.WriteLine("PHHK[{0}] = {1}", i + 1, PHHK[j,i]);
                    }

                    //1.27
                    double uHK = 0;
                    if (nHK == 0 && nBK != 0)
                    {
                        uHK = ncen / nBK;
                    }

                    // 1.28
                    if (nHK != 0 && nBK == 0)
                    {
                        uHK = Math.Abs((ncen - nHK) / nHK);
                    }
                    streamWriter.WriteLine("1.27 - 1.28: uHK = {0}", uHK);

                    // 1.29
                    double dShtrihHK = dm * (1 + GammaHK);
                    streamWriter.WriteLine("1.29: dShtrihHK = {0}", dShtrihHK);

                    // 1.30
                    double GHK = dShtrihHK * Math.Pow(1 / RoHK, (2 - Consth)) * Math.Pow(aHK, 3 - Consth) * Math.Pow(bHK, 3 - 2 * Consth);
                    streamWriter.WriteLine("1.30: GHK = {0}", GHK);

                    //1.31
                    double PShtrihHHK = 0;
                    if (nHK == 0 && nBK != 0)
                    {
                        Sum = 0;
                        for (i = 0; i < n; i++)
                        {
                            Sum += Math.Pow(PHHK[j,i], Constc + 2 - Consth);
                        }
                        PShtrihHHK = Math.Pow((double)1 / n * Sum, 1 / (Constc + 2 - Consth));
                    }

                    // 1.31a
                    if (nHK != 0 && nBK == 0)
                    {
                        Sum = 0;
                        for (i = 0; i < n; i++)
                        {
                            Sum += Math.Pow(PHHK[j,i], (Constc + 2 - Consth) / Conste);
                        }
                        PShtrihHHK = Math.Pow(1 / n * Sum, Conste / (Constc + 2 - Consth));
                    }
                    streamWriter.WriteLine("1.31: PShtrihHHK = {0}", PShtrihHHK);

                    // 1.32
                    double Ro254 = 0;
                    double EpsHK = 0;
                    if (d > 0.0254)
                    {
                        Ro254 = d * RoHK / 0.0254;
                        EpsHK = Math.Pow(ConstE0Shtrih * lambdaE * ConstZita * Ro254 / (Math.PI * ConstKsi * aHK * bHK * bHK), (double)4 / 3);
                    }

                    // 1.33
                    double PHCHK = 0;
                    PHCHK = PhiHK * ConstA * Math.Pow(ConstK, -1 / (Constc + 2 - Consth)) * Math.Pow(lambdaE, (2 - Consth) / (Constc + 2 - Consth)) * Math.Pow(GHK, -1 / (Constc + 2 - Consth)) * Math.Pow(uHK, -Conste / (Constc + 2 - Consth));

                    if (d > 0.0254)
                    {
                        PHCHK *= Math.Pow(1 / EpsHK, 1 / (Constc + 2 - Consth));
                    }
                    streamWriter.WriteLine("1.33: PHCHK = {0}", PHCHK);

                    // 1.34
                    LHK[j] = Math.Pow(PHCHK / PShtrihHHK, (Constc + 2 - Consth) / Conste);
                    streamWriter.WriteLine("1.34: LHK = {0}", LHK[j]);

                    // 1.35
                    double GsharHK = 0;
                    GsharHK = d * Math.Pow(1 / RoHK, 2 - Consth) * Math.Pow(aHK, 3 - Consth) * Math.Pow(bHK, 3 - 2 * Consth);
                    streamWriter.WriteLine("1.35: GsharHK = {0}", GsharHK);

                    // 1.36
                    double PHCsharHK = 0;
                    PHCsharHK = PhiSh * ConstA * Math.Pow(ConstK, -1 / (Constc + 2 - Consth)) * Math.Pow(lambdaE, (2 - Consth) / (Constc + 2 - Consth)) * Math.Pow(GsharHK, -1 / (Constc + 2 - Consth)) * Math.Pow(ushar, -Conste / (Constc + 2 - Consth));
                    streamWriter.WriteLine("1.36: PHCsharHK = {0}", PHCsharHK);

                    // 1.36a
                    double PShtrihHsharHK = 0;
                    if (nHK == 0 && nBK != 0)
                    {
                        Sum = 0;
                        for (i = 0; i < n; i++)
                        {
                            Sum += Math.Pow(PHHK[j,i], (Constc + 2 - Consth) / Conste);
                        }
                        PShtrihHsharHK = Math.Pow(1 / n * Sum, Conste / (Constc + 2 - Consth));
                    }

                    // 1.36b

                    if (nHK != 0 && nBK == 0)
                    {
                        PShtrihHsharHK = PShtrihHHK;
                    }
                    streamWriter.WriteLine("1.36b: PShtrihHsharHK = {0}", PShtrihHsharHK);

                    if (d > 0.0254)
                    {
                        PHCsharHK *= Math.Pow(1 / EpsHK, 1 / (Constc + 2 - Consth));
                    }
                    streamWriter.WriteLine("1.36b: PHCsharHK = {0}", PHCsharHK);

                    // 1.37
                    LsharHK[j] = Math.Pow(PHCsharHK / PShtrihHsharHK, (Constc + 2 - Consth) / Conste);
                    streamWriter.WriteLine("1.37: LsharHK = {0}", LsharHK[j]);

                }
               
                // Раздел 2

                // 1.38 - 1.41
                double []LBKsum = new double[2];
                double []LHKsum = new double[2];
                if (nHK == 0 && nBK != 0)
                {
                    LBKsum[0] = LBK[0] / n;
                    streamWriter.WriteLine("1.38: LBK1sum = {0}", LBKsum[0]);
                    LBKsum[1] = LBK[1] / n;
                    streamWriter.WriteLine("1.39: LBK2sum = {0}", LBKsum[1]);
                    LHKsum[0] = 1 / (Math.Pow(n * Math.Pow(1 / LHK[0], Conste), 1 / Conste));
                    streamWriter.WriteLine("1.40: LHK1sum = {0}", LHKsum[0]);
                    LHKsum[1] = 1 / (Math.Pow(n * Math.Pow(1 / LHK[1], Conste), 1 / Conste));
                    streamWriter.WriteLine("1.41: LHK2sum = {0}", LHKsum[1]);
                }
                if (nHK != 0 && nBK == 0)
                {
                    LBKsum[0] = 1 / Math.Pow(n * Math.Pow(1 / LBK[0], Conste), 1 / Conste);
                    streamWriter.WriteLine("1.38: LBK1sum = {0}", LBKsum[0]);
                    LBKsum[1] = 1 / Math.Pow(n * Math.Pow(1 / LBK[1], Conste), 1 / Conste);
                    streamWriter.WriteLine("1.39: LBK2sum = {0}", LBKsum[1]);
                    LHKsum[0] = LHK[0] / n;
                    streamWriter.WriteLine("1.40: LHK1sum = {0}", LHKsum[0]);
                    LHKsum[1] = LHK[1] / n;
                    streamWriter.WriteLine("1.41: LHK2sum = {0}", LHKsum[1]);
                }

                // 1.42 - 1.43
                double LBKSUM = 0;
                double LHKSUM = 0;
                LBKSUM = 1 / Math.Pow(Math.Pow(1 / LBKsum[0], Conste) + Math.Pow(1 / LBKsum[1], Conste), 1 / Conste);
                streamWriter.WriteLine("1.42: LBKSUM = {0}", LBKSUM);
                LHKSUM = 1 / Math.Pow(Math.Pow(1 / LHKsum[0], Conste) + Math.Pow(1 / LHKsum[1], Conste), 1 / Conste);
                streamWriter.WriteLine("1.43: LHKSUM = {0}", LHKSUM);

                // 1.44 - 1.45
                double LSHARBK = 1 / (1 / LsharBK[0] + 1 / LsharBK[1]);
                double LSHARHK = 1 / (1 / LsharHK[0] + 1 / LsharHK[1]);
                double Lshar = 0;
                Lshar = 1 / (1 / LSHARBK + 1 / LSHARHK);
                streamWriter.WriteLine("1.44 - 1.45: Lshar = {0}", Lshar);

                // 1.46
                double Lpodsh = 0;
                Lpodsh = 1 / Math.Pow(Math.Pow(1 / LBKSUM, Conste) + Math.Pow(1 / LHKSUM, Conste) + n * Math.Pow(1 / Lshar, Conste), 1 / Conste);
                streamWriter.WriteLine("1.46: Lpodsh = {0}", Lpodsh);

                // 1.46a
                double nob = 0;
                if (nHK != 0)
                    nob = nHK;
                if (nBK != 0)
                    nob = nBK;
                streamWriter.WriteLine("1.46a: nob = {0}", nob);

                // 1.47
                double []LBKsumh = new double[2];
                double []LHKsumh = new double[2];
                for (i = 0; i < 2; i++)
                {
                    LBKsumh[i] = (LBKsum[i] * 1E6) / (nob * 60);
                    streamWriter.WriteLine("1.47: LBKsumh[{0}] = {1}", i + 1, LBKsumh[i]);
                    LHKsumh[i] = (LHKsum[i] * 1E6) / (nob * 60);
                    streamWriter.WriteLine("1.47: LHKsumh[{0}] = {1}", i + 1, LHKsumh[i]);
                }

                // 1.47a
                double Lsharsum = 0;
                Lsharsum = Math.Pow(n * Math.Pow(1 / Lshar, Conste), 1 / Conste);
                streamWriter.WriteLine("1.47a: Lsharsum = {0}", Lsharsum);

                // 1.48
                double Lsharsumh = 0;
                Lsharsumh = (Lsharsum * 1E6) / (nob * 60);
                streamWriter.WriteLine("1.48: Lsharsumh = {0}", Lsharsumh);
                double Lpodshh = 0;
                Lpodshh = (Lpodsh * 1E6) / (nob * 60);
                streamWriter.WriteLine("1.48: Lpodshh = {0}", Lpodshh);

                // Раздел 3
                // 3.1
                double []array_pbr = { 1, 0.64, 0.55, 0.47, 0.37, 0.25, 0.22, 0.19, 0.16, 0.12, 0.093, 0.087, 0.08, 0.077 };
                double a1 = 0; // коэффициент a1
                a1 = array_pbr[(int)(pbr - 1)];
                streamWriter.WriteLine("3.1: a1 = {0}", a1);

                // 3.2
                double []array_type = { 1, 1.6, 2, 2, 2, 2.5, 2.5, 3, 3, 2, 4, 1, 1.6, 2, 2, 2.5, 3};
                double a21_BK = 0; // коэффициент a21 для внутреннего кольца
                a21_BK = array_type[(int)(type_BK - 1)];
                streamWriter.WriteLine("3.2: a21 BK = {0}", a21_BK);
                double a21_HK = 0; // коэффициент a21 для наружного кольца
                a21_HK = array_type[(int)(type_HK - 1)];
                streamWriter.WriteLine("3.2: a21 HK = {0}", a21_HK);
                double a21_shar = 0; // коэффициент a21 для шариков
                a21_shar = array_type[(int)(type_shar - 1)];
                streamWriter.WriteLine("3.2: a21 shar = {0}", a21_shar);

                // 3.3
                double []array_method = { 1, 1.5, 3, 3, 4.5, 6};
                double a22_BK = 0; // коэффициент a22 для внутреннего кольца
                a22_BK = array_method[(int)(method_BK - 1)];
                streamWriter.WriteLine("3.3: a22 BK = {0}", a22_BK);
                double a22_HK = 0; // коэффициент a22 для наружного кольца
                a22_HK = array_method[(int)(method_HK - 1)];
                streamWriter.WriteLine("3.3: a22 HK = {0}", a22_HK);
                double a22_shar = 0; // коэффициент a22 для шариков
                a22_shar = array_method[(int)(method_shar - 1)];
                streamWriter.WriteLine("3.3: a22 shar = {0}", a22_shar);

                // 3.4
                double []array_process = { 1.2, 1.2, 1, 1};
                double a23 = 0;
                a23 = array_process[(int)(process - 1)];
                streamWriter.WriteLine("3.4: a23 = {0}", a23);

                // 3.5
                double alpha = 0;
                double beta = 0;
                // 3.5 a24 для внутреннего кольца
                if (type_BK == 3 || type_BK == 8)
                {
                    alpha = 92E-5;
                    beta = 1.6;
                }
                if (type_BK >= 9 && type_BK <= 11)
                {
                    alpha = 113E-5;
                    beta = 1.4;
                }
                double a24_BK = 0;
                a24_BK = Math.Exp(0.1 * (HRC_BK - 60 - alpha * Math.Pow(T - TR, beta)));
                streamWriter.WriteLine("3.5: a24 BK = {0}", a24_BK);

                // 3.5 a24 для наружного кольца
                if (type_HK == 3 || type_HK == 8)
                {
                    alpha = 92E-5;
                    beta = 1.6;
                }
                if (type_HK >= 9 && type_HK <= 11)
                {
                    alpha = 113E-5;
                    beta = 1.4;
                }
                double a24_HK = 0;
                a24_HK = Math.Exp(0.1 * (HRC_HK - 60 - alpha * Math.Pow(T - TR, beta)));
                streamWriter.WriteLine("3.5: a24 HK = {0}", a24_HK);

                // 3.5 a24 для шариков
                if (type_shar == 3 || type_shar == 8)
                {
                    alpha = 92E-5;
                    beta = 1.6;
                }
                if (type_shar >= 9 && type_shar <= 11)
                {
                    alpha = 113E-5;
                    beta = 1.4;
                }
                double a24_shar = 0;
                a24_shar = Math.Exp(0.1 * (HRC_shar - 60 - alpha * Math.Pow(T - TR, beta)));
                streamWriter.WriteLine("3.5: a24 shar = {0}", a24_shar);

                // 3.6
                double a31 = 1;

                // 3.7.1
                double []LambdaBK = new double[2];
                double []hBK= new double[2] { hBK_in[0,0], hBK_in[1,0] };
                for (j = 0; j < 2; j++)
                {
                    for (i = 0; i < n; i++)
                    {
                        if (hBK[j] > hBK_in[j,i])
                            hBK[j] = hBK_in[j,i];
                    }
                    LambdaBK[j] = hBK[j] / (12500 * Math.Sqrt(RaBK * RaBK + Rashar * Rashar));
                    streamWriter.WriteLine("3.7.1: LambdaBK[{0}] = {1}", j + 1, LambdaBK[j]);
                }
                double []a321BK = new double[2];
                for (j = 0; j < 2; j++)
                {
                    if (LambdaBK[j] <= 2.04)
                    {
                        a321BK[j] = -2.310 * Math.Pow(LambdaBK[j], 3) + 8.833 * Math.Pow(LambdaBK[j], 2) - 8.756 * LambdaBK[j] + 2.712;
                    }
                    else
                    {
                        a321BK[j] = 0.017 * Math.Pow(LambdaBK[j], 3) - 0.303 * Math.Pow(LambdaBK[j], 2) + 1.721 * LambdaBK[j] - 0.393;
                    }
                    streamWriter.WriteLine("3.7.1: a321BK[{0}] = {1}", j + 1, a321BK[j]);
                }

                // 3.7.2
                double []LambdaHK = new double[2];
                double []hHK = new double[2] { hHK_in[0,0], hHK_in[1,0] };

                for (j = 0; j < 2; j++)
                {
                    for (i = 0; i < n; i++)
                    {
                        if (hHK[j] > hHK_in[j,i])
                            hHK[j] = hHK_in[j,i];
                    }
                    LambdaHK[j] = hHK[j] / (12500 * Math.Sqrt(RaHK * RaHK + Rashar * Rashar));
                    streamWriter.WriteLine("3.7.2: LambdaHK[{0}]= {1}", j + 1, LambdaHK[j]);
                }

                // 3.7.2
                double []a321HK = new double[2];
                for (j = 0; j < 2; j++)
                {
                    if (LambdaHK[j] <= 2.04)
                    {
                        a321HK[j] = -2.31 * Math.Pow(LambdaHK[j], 3) + 8.833 * Math.Pow(LambdaHK[j], 2) - 8.756 * LambdaHK[j] + 2.712;
                    }
                    else
                    {
                        a321HK[j] = 0.017 * Math.Pow(LambdaHK[j], 3) - 0.303 * Math.Pow(LambdaHK[j], 2) + 1.721 * LambdaHK[j] - 0.393;
                    }
                    streamWriter.WriteLine("3.7.2: a321HK[{0}] = {1}", j + 1, a321HK[j]);
                }

                // 3.8
                double a322 = 1;

                // 3.9
                double a323 = 0;
                a323 = 1.8 * Math.Pow(1.39 * FR - 4.11, -0.25);
                streamWriter.WriteLine("3.9: a323 = {0}", a323);

                // Раздел 4
                // 1.49 - 1.50
                double []LBKM = new double[2];
                for (i = 0; i < 2; i++)
                {
                    LBKM[i] = a1 * a21_BK * a22_BK * a23 * a24_BK * a31 * a321BK[i] * a322 * a323 * LBKsum[i];
                    streamWriter.WriteLine("1.49 - 1.50: LBKM[{0}] = {1}", i + 1, LBKM[i]);
                }

                // 1.51
                double []LsharBKM = new double[2];
                for (i = 0; i < 2; i++)
                {
                    LsharBKM[i] = a1 * a21_shar * a22_shar * a23 * a24_shar * a31 * a321BK[i] * a322 * a323 * LSHARBK;
                    streamWriter.WriteLine("1.51: LsharBKM[{0}] = {1}", i + 1, LsharBKM[i]);
                }

                // 1.52
                double []LsharHKM = new double[2];
                for (i = 0; i < 2; i++)
                {
                    LsharHKM[i] = a1 * a21_shar * a22_shar * a23 * a24_shar * a31 * a321HK[i] * a322 * a323 * LSHARHK;
                    streamWriter.WriteLine("1.52: LsharHKM[{0}] = {1}", i + 1, LsharHKM[i]);
                }

                // 1.53 - 1.54
                double []LHKM = new double[2];
                for (i = 0; i < 2; i++)
                {
                    LHKM[i] = a1 * a21_HK * a22_HK * a23 * a24_HK * a31 * a321HK[i] * a322 * a323 * LHKsum[i];
                    streamWriter.WriteLine("1.53 - 1.54: LHKM[{0}] = {1}", i + 1, LHKM[i]);
                }

                // 1.55
                double LsharM = 0;
                LsharM = 1 / (1 / LsharBKM[0] + 1 / LsharBKM[1] + 1 / LsharHKM[0] + 1 / LsharHKM[1]);
                streamWriter.WriteLine("1.55: LsharM = {0}", LsharM);

                // 1.56
                double LsharSUMM = 0;
                LsharSUMM = 1 / Math.Pow(n * Math.Pow(1 / LsharM, Conste), 1 / Conste);
                streamWriter.WriteLine("1.56: LsharSUMM = {0}", LsharSUMM);

                // 1.57
                double LBKM_res = 0;
                LBKM_res = 1 / Math.Pow(Math.Pow(1 / LBKM[0], Conste) + Math.Pow(1 / LBKM[1], Conste), 1 / Conste);
                streamWriter.WriteLine("1.57: LBKM_res = {0}", LBKM_res);

                // 1.58
                double LHKM_res = 0;
                LHKM_res = 1 / Math.Pow(Math.Pow(1 / LHKM[0], Conste) + Math.Pow(1 / LHKM[1], Conste), 1 / Conste);
                streamWriter.WriteLine("1.58: LHKM_res = {0}", LHKM_res);

                // 1.59
                double LpodshM = 0;
                LpodshM = Math.Pow(Math.Pow(1 / LBKM_res, Conste) + Math.Pow(1 / LHKM_res, Conste) + Consth * Math.Pow(1 / LsharM, Conste), 1 / Conste);
                streamWriter.WriteLine("1.59: LpodshM = {0}", LpodshM);

                // 1.60
                double LsharSUMMh = 0;
                LsharSUMMh = LsharSUMM * 1E6 / (nob * 60);
                streamWriter.WriteLine("1.60: LsharSUMMh = {0}", LsharSUMMh);

                // 1.61 - 1.62
                double []LBKMh = new double[2];
                for (i = 0; i < 2; i++)
                {
                    LBKMh[i] = LBKM[i] * 1E6 / (nob * 60);
                    streamWriter.WriteLine("1.61 - 1.62: LBKMh[{0}] = {1}", i + 1, LBKMh[i]);
                }

                // 1.63 - 1.64
                double []LHKMh = new double[2];
                for (i = 0; i < 2; i++)
                {
                    LHKMh[i] = LHKM[i] * 1E6 / (nob * 60);
                    streamWriter.WriteLine("1.63 - 1.64: LHKMh[{0}] = {1}", i + 1, LHKMh[i]);
                }

                // 1.65
                double LpodshMh = 0;
                LpodshMh = LpodshM * 1E6 / (nob * 60);
                streamWriter.WriteLine("1.65: LpodshMh = {0}", LpodshMh);
                streamWriter.Close();
                /*
                // запись в файл
                ofstream f_out("OUT.TXT"); // открытие файла

                if (!f_out) // если файл не открыт
                {
                    cout << "Ошибка открытия файла OUT.TXT\n"; // сообщить об этом
                    system("pause");
                    return 0;
                }

                // вывод результатов в файл OUT.TXT
                f_out << left << "Расчет долговечностей" << endl;
                f_out << "Номинальная долговечность дорожек качения" << endl;
                f_out << LBKsumh[0] << endl;
                f_out << LBKsumh[1] << endl;
                f_out << LHKsumh[0] << endl;
                f_out << LHKsumh[1] << endl;
                f_out << setw(60) << "Номинальная долговечность комплекта шариков" << setw(20) << Lsharsumh << endl;
                f_out << setw(60) << "Номинальная долговечность подшипника" << setw(20) << Lpodsh << endl;
                f_out << endl << endl << "Модифицированная долговечность" << endl;
                f_out << "Модифицированная долговечность дорожек качения" << endl;
                f_out << LBKMh[0] << endl;
                f_out << LBKMh[1] << endl;
                f_out << LHKMh[0] << endl;
                f_out << LHKMh[1] << endl;
                f_out << setw(60) << "Модифицированная долговечность комплекта шариков" << setw(20) << LsharSUMMh << endl;
                f_out << setw(60) << "Модифицированная долговечность подшипника" << setw(20) << LpodshMh << endl;
                */

                streamWriter.Close();
                fs.Close();
                MessageBox.Show("Файл TEMP успешно сохранен");
            }
            catch
            {
                MessageBox.Show("Ошибка при сохранении файла TEMP!");
            }
            streamWriter.Close();
            Directory.SetCurrentDirectory(oldPath);
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Save data file (*.dat)|*.dat";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                string jsonString = JsonSerializer.Serialize(ArraySHAR);
                File.WriteAllText(saveFileDialog1.FileName, jsonString);

                //FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                //StreamWriter streamWriter = new StreamWriter(fs);
                //try
                //{
                //    // запись в файл dataGridView1
                //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
                //    {
                //        streamWriter.Write(String.Format("{0, -60} {1}", dataGridView1.Rows[i].Cells[0].Value, dataGridView1.Rows[i].Cells[1].Value));
                //        streamWriter.WriteLine();
                //    }
                //    // запись в файл dataGridView2
                //    for (int i = 0; i < dataGridView2.Rows.Count; i++)
                //    {
                //        streamWriter.Write(String.Format("{0, -60} {1}", dataGridView2.Rows[i].Cells[0].Value, dataGridView2.Rows[i].Cells[1].Value));
                //        streamWriter.WriteLine();
                //    }
                //    streamWriter.Close();
                //    fs.Close();
                //    // MessageBox.Show("Файл успешно сохранен");
                //}
                //catch
                //{
                //    MessageBox.Show("Ошибка при сохранении файла!");
                //}
            }
        }
    }
}
